
# Get version from .versionrc
$read_version = Select-String -Pattern "`"version`": `"(.*)`"" -Path $pwd\.versionrc
$version = $read_version.matches.groups[1].value

# Modifies package version
(Get-Content -path $pwd\config\package-spec.pbs -Raw) -replace "0.0.0.0","$version.0" | Set-Content -Path $pwd\config\package-spec.pbs




