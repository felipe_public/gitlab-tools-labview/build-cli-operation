# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## 1.0.0 (2021-03-24)


### Features

* adds LICENSE ([784bbb5](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/784bbb59948c364ab737c92eef653c32e5d92987))
* adds package builder configuration file ([34e2972](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/34e297251cff49ea6a21026ed65c8507b3d6e00b))
* creates the buildcustom operation class ([a3120dc](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/a3120dc641077dd53d2b34cc188cf06e5aa87352))


### Bug Fixes

* failing vi in static code test - error handling ([f84011c](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/f84011c0405abd538b051cf948f97f1b03b894d4))
* include missing files in package builder ([4a87076](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/4a87076b726bd569ce64123ab988aae75c918084))


### Docs

* adds avatar file ([62a7a99](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/62a7a99b2b91ee1e7649c7f53ca2f9a321b56e53))
* adds readme file ([e92b346](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/e92b34617f997fafd48c5282dd94ab124bb6c5ff))
* modifies README to add new parameter version and modifies project ([d251a7a](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/d251a7a0a3adcc4d40a02ac9b4287342a0b15837))


### Tests

* **static:** adds vi analyzer configuration ([05f8fb4](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/05f8fb40a6749196a5676841c8fd7da9baaa17da))
* **static:** removes test as high rank ([931c9a3](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/931c9a3f7875c58a1b43c9e83de8e313fd4aed5d))
* **unit:** adds support project for build simulation ([f9b3e9c](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/f9b3e9cd117bdbd04e278438a3b5f639eabda366))
* **unit:** adds support projects for builds ([8656540](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/865654062d09544f9ab3f44e41f96ca88ca107f2))
* **unit:** adds test support project ([bab4d4b](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/bab4d4bd523fcadc5dab3c47fe7ede7aad4bb55a))
* **unit:** adds tests for add arguments (version) ([a5e3baa](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/a5e3baae2e211184d79470cc82f46f1164ec5399))
* **unit:** fixes relative path in path constant ([1947b6e](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/1947b6eb117d272bf3c374bd266ff2689a19d86b))


### CI

* adds automatic versioning configuration ([293d80a](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/293d80a8862679d131e891a2cd077836fd6de478))
* adds extends to package job ([8286d13](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/8286d13c131bf920f62c817fbca77cf7e6a022ae))
* adds pipeline configuration file ([892e344](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/892e3443d42c7e15b81785597ba714678384704e))
* fix on the script path variable ([2da9484](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/2da94844a2145df992b4816978c5bc38613171d5))
* includes upload job ([2ca6090](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/2ca60907b602b4da9bd20b2ffbbb45e406b22b69))
* includes version script ([4da9115](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/4da911581a08ddc05d70f4ebf0716cf542185e8c))
* removes dependencies job ([b9be924](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/b9be924415d151483ab41abe586ef504443b086c))
* removes tag from release job ([294adbe](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/294adbeddf6ea7f155794e31997db3980aa6a99f))
* sets first release in .versionrc ([a7b4383](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/a7b4383033b3b63ecf985ca7e529cc9d0fab1196))
* this script gets version and then set version in package spec ([6f67512](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/6f675121cc79e7179be5a504bc11482fe5707c45))
* updates the version command with grep compatible flags ([bfa5e23](https://gitlab.com/felipe_public/gitlab-tools-labview/build-cli-operation/commit/bfa5e23a9efde5e7e985d22d91f96ef5518c68e3))
